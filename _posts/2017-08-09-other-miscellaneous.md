---
layout: "post"
title: "Other miscellaneous"
date: "2010-07-21"
tenure: "2010 – present"
category: "4. extracurricular"
subtitle: "Catch-all"
---

- Part of Cognizant's CSR wing, Outreach; taught Computer Literacy to children from government schools.
- Psychology enthusiast, with a goal to write a paper on Psychology and Ethical Marketing, someday.
- Participate in and judge debates that focus on exchange of ideas, rather than being battle of egos.
- Cycling enthusiast.
