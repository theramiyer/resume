---
layout: "post"
title: "Alpha Arts and Science College"
subtitle: "B.Sc. (Electronics and Communications)"
date: "2010-07-21"
tenure: "2007 – 2010"
category: "3. education"
more: true
---

- University of Madras rank-holder in Major (13<sup>th</sup>).
- Recognised as the Best Outgoing Student (2007 -- ’10 batch).
- Served as the President of the Students' Forum.
<!--more-->
- Served as the Editor of the yearly departmental magazine.
- Participated and won awards in several national- and state-level intercollegiate competitions.
