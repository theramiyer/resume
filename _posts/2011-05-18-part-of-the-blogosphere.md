---
layout: "post"
title: "Part of the Blogosphere"
date: "2011-05-18"
tenure: "2011 – present"
category: "4. extracurricular"
subtitle: "Tech, literary and general"
---

- [Collection of short stories](http://ramiyer.me).
- [Tech blog focussed on beginners in IT](http://tyro.ramiyer.me).
- [Blog of opinions, and on life in general](http://blog.ramiyer.me).
