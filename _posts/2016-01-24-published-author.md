---
layout: "post"
title: "Published Author"
date: "2016-01-24"
tenure: "2016 – present"
category: "4. extracurricular"
subtitle: "Fiction and non-fiction books"
---

- Published author of a literary fiction novelette, _The Damp Roman Candle_.
- Wrote a book on an analysis of Multi-level Marketing, _Pyramid on My Ceiling_.
- Working on the publication of a fiction novel, _The Restrained Fireball_.
