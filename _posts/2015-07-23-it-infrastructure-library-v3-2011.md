---
layout: "post"
title: "ITIL&reg; 2011 Foundation"
date: "2013-09-14"
tenure: "2013"
category: "2. achievements and certifications"
subtitle: "ITIL&reg; Foundation Certificate in IT Service Management"
---
- [Certificate Authentication](https://www.exin.com/NL/en/certificate-authentication) number **4843583.20192091**.
<!--more-->
