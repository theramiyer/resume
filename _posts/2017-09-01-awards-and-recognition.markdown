---
layout: "post"
title: "Awards and Recognition"
date: "2010-07-21"
tenure: "2010 – 2017"
category: "2. achievements and certifications"
subtitle: "Cognizant Technology Solutions"
---

- Associate of the Quarter (Q4, 2016; Q2, 2011)
- Best Team Automation/Innovation (team award, 2017)
- Outstanding Dependability (2016)
- Techie Mavericks (team award, 2016)
- Best Tutor (2015)
- The Leading Light (2014)
